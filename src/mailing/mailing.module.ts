import {Module} from "@nestjs/common";
import {MailingService} from "./mailing.service";
import {MailerModule} from "@nestjs-modules/mailer";
import {join} from "path";
import {HandlebarsAdapter} from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {TypeOrmModule} from "@nestjs/typeorm";
import {MailLogEntity} from "./mail-log.entity";
import {MailingController} from "./mailing.controller";

@Module({
    imports: [
        TypeOrmModule.forFeature([MailLogEntity]),
        MailerModule.forRootAsync({
            useFactory: async (config: ConfigService) => ({
                // transport: 'smtps://user@example.com:topsecret@smtp.example.com',
                // or
                transport: {
                    host: config.get('MAIL_HOST'),
                    secure: false,
                    auth: {
                        user: config.get('MAIL_USER'),
                        pass: config.get('MAIL_PASSWORD'),
                    },
                },
                defaults: {
                    from: `"No Reply" <${config.get('MAIL_FROM')}>`,
                },
                template: {
                    dir: join(__dirname, 'templates'),
                    adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
                    options: {
                        strict: true,
                    },
                },
            }),
            inject: [ConfigService]
        })],
    controllers: [MailingController],
    providers: [MailingService],
    exports: [MailingService]
})
export class MailingModule {

}
