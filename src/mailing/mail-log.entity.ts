import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

export enum LogState {
    STARTED,
    SENT,
    FAILED
}

@Entity()
export class MailLogEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({type: "timestamp", nullable: false})
    timeStamp: string;

    @Column({nullable: false})
    userId: number;

    @Column({
        type: "enum",
        enum: LogState,
        nullable: false
    })
    state: LogState
}
