import {Controller} from "@nestjs/common";
import {MailingService} from "./mailing.service";
import {MessagePattern} from "@nestjs/microservices";
import {UserDto} from "./user.dto";

@Controller('mailing')
export class MailingController {
    constructor(
        private readonly mailingService: MailingService
    ) {
    }

    @MessagePattern({cmd: 'sendMail'})
    addSubscriber(userDto: UserDto) {
        return this.mailingService.sendUserConfirmation(userDto);
    }

    // @MessagePattern({cmd: 'get-all-subscribers'})
    // getAllSubscribers() {
    //     return this.subscribersService.getAllSubscribers();
    // }
}
