import {Injectable} from "@nestjs/common";
import {MailerService} from "@nestjs-modules/mailer";
import {UserDto} from "./user.dto";
import {Repository} from "typeorm";
import {MailLogEntity} from "./mail-log.entity";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class MailingService {
    constructor(
        @InjectRepository(MailLogEntity)
        private mailLogRepository: Repository<MailLogEntity>,
        private mailerService: MailerService) {
    }

    async sendUserConfirmation(user: UserDto) {
        const redirectUrl = `localhost:3000/verification/${user.token}`;

        console.log('Sending data with: ' + redirectUrl);
        return this.mailerService.sendMail({
            to: user.email,
            // from: '"Support Team" <support@example.com>', // override default from
            subject: 'Welcome to Nice App! Confirm your Email',
            template: './confirmation', // `.hbs` extension is appended automatically
            context: { // ✏️ filling curly brackets with content
                name: user.name,
                login: user.login,
                verifyUrl: redirectUrl
            },
        });
    }
}
