export class UserDto {
    name: string;
    login: string;
    email: string;
    token: string;
}
