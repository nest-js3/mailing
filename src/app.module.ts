import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import * as Joi from 'joi';
import {ConfigModule} from "@nestjs/config";
import {MailingModule} from "./mailing/mailing.module";
import {DatabaseModule} from "./database/database.module";

@Module({
    imports: [
        MailingModule,
        ConfigModule.forRoot({
            isGlobal: true,
            validationSchema: Joi.object({
                POSTGRES_HOST: Joi.string().required(),
                POSTGRES_PORT: Joi.number().required(),
                POSTGRES_USER: Joi.string().required(),
                POSTGRES_PASSWORD: Joi.string().required(),
                POSTGRES_DB: Joi.string().required(),
                PORT: Joi.number(),

                MAIL_HOST: Joi.string().required(),
                MAIL_USER: Joi.string().required(),
                MAIL_PASSWORD: Joi.string().required(),
                MAIL_FROM: Joi.string()
            })
        }),
        DatabaseModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
